import type {Listener} from "@arabesque/core";
import type {ConsumerConfig, KafkaConfig} from "kafkajs";

import {Kafka} from "kafkajs";

export type Configuration = {
    client: KafkaConfig;
    consumer: ConsumerConfig;
};

export const createListener = <Message>(configuration: Configuration): Listener<string, Message> => {
    return (topic, handle) => {
        const {client: clientConfiguration, consumer: consumerConfiguration} = configuration;

        const kafkaClient = new Kafka(clientConfiguration);

        const consumer = kafkaClient.consumer(consumerConfiguration);

        return consumer.subscribe({
            topic
        }).then(() => {
            return consumer.run({
                eachMessage: (payload) => {
                    const rawMessage = payload.message;
                    const rawMessageValue = rawMessage.value;

                    if (rawMessageValue) {
                        const message: Message = JSON.parse(rawMessageValue.toString());

                        return handle(message).then(() => undefined);
                    }

                    return Promise.resolve();
                }
            });
        }).then(() => {
            return () => consumer.disconnect();
        });
    };
}