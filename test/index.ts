import * as tape from "tape";
import {spy, stub} from "sinon";
import {createListener} from "../src";
import * as KafkaModule from "kafkajs";
import {Consumer, ConsumerRunConfig, Kafka} from "kafkajs";

tape('listener', ({same, end}) => {
    let passedRunConfigEachMessage: ConsumerRunConfig["eachMessage"];

    const consumer: {
        subscribe: Consumer["subscribe"],
        run: Consumer["run"],
        disconnect: Consumer["disconnect"]
    } = {
        subscribe: () => {
            return Promise.resolve();
        },
        run: (config) => {
            passedRunConfigEachMessage = config?.eachMessage;

            return Promise.resolve();
        },
        disconnect: () => {
            return Promise.resolve();
        }
    };

    const spiedKafka = spy(KafkaModule, "Kafka");
    const stubbedConsumer = stub(Kafka.prototype, "consumer").callsFake(() => consumer as Consumer);
    const spiedConsumerSubscribe = spy(consumer, "subscribe");
    const spiedConsumerDisconnect = spy(consumer, "disconnect");

    const handler: any = () => {
        return Promise.resolve();
    };

    const spiedHandler = spy(handler);

    const listener = createListener({
        client: {
            brokers: [
                'broker'
            ]
        },
        consumer: {
            groupId: 'groupId'
        }
    });

    return listener('topic', spiedHandler).then(async (stop) => {
        same(spiedKafka.firstCall.args, [{brokers: ['broker']}], 'should instantiate the Kafka client with the passed client configuration');
        same(stubbedConsumer.firstCall.args, [{groupId: 'groupId'}], 'should instantiate the Kafka consumer with the passed consumer configuration');
        same(spiedConsumerSubscribe.firstCall.args, [{topic: 'topic'}], 'should subscribe the Kafka consumer to the topic');

        const messageTestCases: Array<{
            value: any,
            called: boolean
        }> = [
            {
                value: {foo: 'bar'},
                called: true
            },
            {
                value: null,
                called: false
            }
        ];

        for (const {value, called} of messageTestCases) {
            await passedRunConfigEachMessage!({
                topic: 'foo',
                message: {
                    headers: {},
                    value: value ? Buffer.from(JSON.stringify(value)) : null,
                    key: Buffer.from(''),
                    attributes: 1,
                    offset: '1',
                    size: 1,
                    timestamp: '1'
                },
                partition: 1
            });

            const message = called ?
                `should pass ${JSON.stringify(value)} message to the handler` :
                `should not pass ${value} message to the handler`;

            same(spiedHandler.called, called, message);

            spiedHandler.resetHistory();
        }

        return stop().then(() => {
            same(spiedConsumerDisconnect.called, true, 'should disconnect the consumer when stopped');
        });
    }).finally(() => {
        stubbedConsumer.restore();

        end();
    });
});