# Kafka listener for [Arabesque](https://www.npmjs.com/package/@arabesque/core)

[![NPM version][npm-image]][npm-url] [![Coverage Status][coverage-image]][coverage-url]

## Prerequisites

* [KafkaJS](https://www.npmjs.com/package/kafkajs) version 1.15.0 and up

## Getting started

Kafka listener for Arabesque consists of a type and a factory that returns an Arabesque Listener.

### Configuration

```ts
import type {ConsumerConfig, KafkaConfig} from "kafkajs";

declare type Configuration = {
    client: KafkaConfig;
    consumer: ConsumerConfig;
};
```

The configuration supported by the listener factory:

* client: The configuration passed to the KafkaJS client - please refer
  to [KafkaJS documentation](https://kafka.js.org/docs/getting-started) for details
* consumer: The configuration passed to the KafkaJS consumer - please refer
  to [KafkaJS documentation](https://kafka.js.org/docs/getting-started) for details

### createListener

```ts
declare const createListener: <Message>(configuration: Configuration) => Listener<Channel, Message>;
```

The factory that creates a Kafka listener. The returned listener is guaranteed to disconnect the consumer when the
application is stopped.

## Usage

```ts
import {createApplication} from "@arabesque/core";
import {createListener} from "@arabesque/listener-kafka";

const listener = createListener({
  client: {
    brokers: [
      'broker:29092'
    ]
  },
  consumer: {
    groupId: 'foo',
  }
});
const application = createApplication(listener);

application.listen('bar');
```

## Contributing

* Fork this repository
* Code
* Implement tests using [tape](https://github.com/substack/tape)
* Issue a merge request keeping in mind that all pull requests must reference an issue in the issue queue

[npm-image]: https://badge.fury.io/js/@arabesque%2Flistener-kafka.svg

[npm-url]: https://www.npmjs.com/package/@arabesque/listener-kafka

[coverage-image]: https://coveralls.io/repos/gitlab/arabesque/listener-kafka/badge.svg

[coverage-url]: https://coveralls.io/gitlab/arabesque/listener-kafka